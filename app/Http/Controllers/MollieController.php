<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Mollie\Laravel\Facades\Mollie;
use App\Mail\Paid;
use App\Mail\Order;

class MollieController extends Controller
{
    public function Mollie (Request $request){

        $mollie = new \Mollie_API_Client();
        $mollie->setApiKey('test_qwHzpzzFgDCFtMMhbKJUgrPyF5dg77');

        $customer = $mollie->customers->create([
            "name"  => $request['name'],
            "email" => $request['email']
        ]);

        Mollie::api()->payments()->create([
            'amount'        => $request['payment'],          // 1 cent or higher
            'customerId'    => $customer->id,
            'recurringType' => 'first',       // important
            'description'   => 'fijnsysteem',
            'method'        => $request['method'],
            'redirectUrl'   => 'http://localhost/project/public/product',
            'webhookUrl'    => 'http://localhost/project/public/payment',

        ]);




    }
    public function payment(Request $request)
    {
        $payment = Mollie::api()->payments()->get($_POST["id"]);


        if ($payment->isPaid()){


            Mail::to('customer@live.nl')->send(new Paid);
            Mail::to('info@uselessstuff.com')->send(new Order);
            $request->session()->flush();
        }
        else if($payment->isOpen() == False){
            return Redirect::to($payment->links->paymentUrl);
        }

        return view('product.paid');
    }
}
