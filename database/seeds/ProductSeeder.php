<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'imagePath' => 'http://thetinylife.com/wp-content/uploads/2011/06/cardboard-box-open-lg.jpg',
           'name' => 'Doos',
           'description' => 'Een doos test',
            'price' => 1.50
        ]);

        DB::table('products')->insert([
            'imagePath' => 'http://thetinylife.com/wp-content/uploads/2011/06/cardboard-box-open-lg.jpg',
            'name' => 'Doos 2',
            'description' => 'Weer een test',
            'price' => 4.20
        ]);

        DB::table('products')->insert([
            'imagePath' => 'http://thetinylife.com/wp-content/uploads/2011/06/cardboard-box-open-lg.jpg',
            'name' => 'Doos 3',
            'description' => 'Een doos test',
            'price' => 3.10
        ]);

        DB::table('products')->insert([
            'imagePath' => 'http://thetinylife.com/wp-content/uploads/2011/06/cardboard-box-open-lg.jpg',
            'name' => 'Doos 4',
            'description' => 'Nog een doos test',
            'price' => 2.50
        ]);

        DB::table('products')->insert([
            'imagePath' => 'http://thetinylife.com/wp-content/uploads/2011/06/cardboard-box-open-lg.jpg',
            'name' => 'Doos 5',
            'description' => 'Laatste doos test',
            'price' => 2.80
        ]);
    }
}
