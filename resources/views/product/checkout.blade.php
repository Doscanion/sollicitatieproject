@extends('master')

@section('content')
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
            <h1>Checkout</h1>

            <form action="{{route('mollie')}}" method = "post" id="checkout-form">
                {{csrf_field()}}
                <h4>Total: €{{str_replace(".", ",",  number_format($total, 2))}}</h4>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="formgroup">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="formgroup">
                            <label for="surname">Surname</label>
                            <input type="text" name="surname" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="formgroup">
                            <label for="tel">Tel.</label>
                            <input type="tel" name="tel" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="formgroup">
                            <label for="address">Address</label>
                            <input type="text" name="address" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="formgroup">
                            <label for="city">City</label>
                            <input type="text" name="city" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="formgroup">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="formgroup">
                            <label for="method">Payment Method</label>
                            <select name="method" class="form-control" required>
                                <option value="creditcard">Creditcard</option>
                                <option value="sofort">Sofort</option>
                                <option value="ideal">Ideal</option>
                                <option value="mistercash">MisterCash</option>
                                <option value="kbc">KBC/CBC</option>
                                <option value="belfius">Belfius</option>
                                {{--<option value="banktransfer">Banktransfer</option>--}}
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="payment" class="form-control" value="{{$total}}" required>
                    <button type="submit" class="pay-button">Buy</button>

                </div>
            </form>
        </div>
    </div>
@endsection