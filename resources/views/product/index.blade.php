@extends('master')

@section('content')
    @foreach($products->chunk(3) as $productChunk)
        <div class="row">
            @foreach($productChunk as $product)
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img src="{{ $product->imagePath }}" alt="...">
                        <div class="caption">
                            <h3>{{$product->name}}</h3>
                            <p class="description">{{$product->description}}</p>
                            <div class = 'clearfix'>
                                <div class="pull-left price">€{{str_replace(".", ",", $product->price)}}</div>
                                <a href="{{ route('product.addToCart', ['id' => $product->id]) }}" class="btn btn-success pull-right" onclick="customBoxes({{$product->id}})" role="button">Add to Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
@endsection