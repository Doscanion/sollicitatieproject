<nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('product.index') }}">Useless Stuff</a>
        </div>

        <div class="custombutton">
            <button id="customButton" class="dropbtn" onclick="myBoxes()">Boxes</button>
            <div id="customDropdown" class="dropdown-content">
                <a href="#">More Boxes</a>
                <a href="#">Other kind of Boxes</a>
                <a href="#">Crates</a>
            </div>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="{{ route('product.shoppingCart') }}">
                    <span class="badge">{{Session::has('cart') ? Session::get('cart')->totalQty : ''}}</span>
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    Shopping Cart
                </a>
            </li>
        </ul>
    </div>
</nav>